<?php
/*
Template Name: Home Page
*/
get_header();
if( $theme_options['home-switch-to-page-header-status'] == true ) {
?>
<!-- Global Bar -->
<style><?php echo '.button-custom { padding: 15px 20px 15px !important; }'; ?></style>
<div class="jumbotron" style="background:url(<?php if(isset($theme_options['home-header-background-img']['url']) && $theme_options['home-header-background-img']['url'] != '') { echo esc_url($theme_options['home-header-background-img']['url']); } ?>)!important;background-size:cover!important; background-position: 50% 50%;
    background-repeat: no-repeat; padding: 0px!important;">
  <div class="jumbotron-bg-color" style="padding: <?php echo $theme_options['home-header-background-padding']; ?>px 0;">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 padding-jumbotron">
          <h1 id="glob-msg-box" class="bigtext"><?php echo $theme_options['home-header-main-title']; ?></h1>
          <p class="titletag_dec"><?php echo $theme_options['home-header-sub-title']; ?></p>

          <?php if( $theme_options['home-search-form-status'] == true ) { ?>
          <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 search-margin-top">
            <div class="global-search home">
              <?php get_template_part( 'search', 'home' ); ?>
            </div>
          </div>
          <?php } ?>

        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<!--Home Help Section-->
<?php if( $theme_options['home-help-section-status'] == true ) { ?>
<div class="clearfix"></div>
<div class="">
  <div class="container content-wrapper top-body-shadow body-content">
    <div class="row">
      <div style="margin-top:40px">
        <div class="col-md-12 col-sm-12" style="text-align:center;">
          <h2 class="margin-btm-25 uppercase"><?php echo $theme_options['home-help-section-title-main']; ?></h2>
          <p class="custom-size"><?php echo $theme_options['home-help-section-msg-short']; ?></p>
        </div>
        <div class="col-md-12 col-sm-12">
          <div class="loop-help-desk">
            <?php
        $args = array(
	 				'post_type' => 'manual_hp_block',
					'posts_per_page' => '3',
					'orderby' => 'menu_order',
					'post_status' => 'publish',
					'order' => 'ASC'
				);
		$wp_query = new WP_Query($args);
		if($wp_query->have_posts()) {
		while($wp_query->have_posts()) { $wp_query->the_post();
		?> <a href="<?php echo get_post_meta( $post->ID, '_manual_hpblock_link', true ); ?>">
            <div class="counter-text hvr-float">
              <div class="browse-help-desk" style="background:<?php echo get_post_meta( $post->ID, '_manual_hpblock_bg_color', true ); ?>;">
                <div class="browse-help-desk-div">
                <?php
				$icon_image_url = get_post_meta( get_the_ID(), '_manual_hpblock_custom_icon', 1 );
				if( $icon_image_url != '' ) {
					echo '<img src="'.$icon_image_url.'">';
				} else {?>
                <i class="<?php echo get_post_meta( $post->ID, '_manual_hpblock_icon', true ); ?> i-fa" style="color:<?php echo get_post_meta( $post->ID, '_manual_hpblock_icon_color', true ); ?>;"></i>
                <?php } ?>
                </div>
                <div class="m-and-p">
                  <h5 style="color:<?php echo get_post_meta( $post->ID, '_manual_hpblock_text_color', true ); ?>;">
                    <?php the_title(); ?>
                  </h5>
                  <p style="color:<?php echo get_post_meta( $post->ID, '_manual_hpblock_text_color', true ); ?>;"><?php echo get_post_meta( $post->ID, '_manual_hpblock_text', true ); ?></p>
                  <?php if( get_post_meta( $post->ID, '_manual_hpblock_link', true ) != '' ) { ?>
                  <p style="color:<?php echo get_post_meta( $post->ID, '_manual_hpblock_text_color', true ); ?>;">
                    <span class="custom-link hvr-icon-wobble-horizontal" style="color:<?php echo get_post_meta( $post->ID, '_manual_hpblock_link_color', true ); ?>!important;">
                    <?php
					if( get_post_meta( $post->ID, '_manual_hpblock_link_text', true ) != '' ) {
						echo get_post_meta( $post->ID, '_manual_hpblock_link_text', true );
					} else {
						esc_html_e( 'Browse', 'manual' ); echo '&nbsp;'; the_title();
					}
					?>
                    </span>
                  </p>
                  <?php } ?>
                </div>
              </div>
            </div>
            </a>
            <?php
		}
		}
		wp_reset_query();
		?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<!--Testo-->
<?php if( $theme_options['home-testimonials-status'] == true ) { ?>
<div class="homepg-seprator-bg parallax-window" <?php if( isset($theme_options['testimonials-plx-url']['url']) && $theme_options['testimonials-plx-url']['url'] != '' ) { ?> data-image-src="<?php echo esc_url($theme_options['testimonials-plx-url']['url']); ?>" data-parallax="scroll" <?php } ?>>
  <div style="background: rgba(55, 56, 53, 0.79);  padding:95px 0px;">
    <div class="container">
      <div class="row">
        <div class="margin-15">
          <div class="home-testo-desk">
            <?php
        $args = array(
	 				'post_type' => 'manual_tmal_block',
					'posts_per_page' => '-1',
					'orderby' => 'menu_order',
					'post_status' => 'publish',
					'order' => 'DESC'
				);
		$wp_query = new WP_Query($args);
		if($wp_query->have_posts()) {
			$i = 0;
			while($wp_query->have_posts()) { $wp_query->the_post();
		?>
            <div class="col-md-12 col-sm-12  testimonial">
              <div class="testimonial-text">
                <div class="testimonial-quote">
                  <p style="color:#EDEDED!important;font-size: 26px;line-height: 1.666666666666667em;font-weight: 400;"><?php echo get_post_meta( $post->ID, '_manual_hpblock_text', true ); ?></p>
                </div>
                <div class="testimonial-cite text-white-color"><?php echo get_post_meta( $post->ID, '_manual_hpblock_name', true ); ?></div>
              </div>
            </div>
            <?php
			}
		}
		wp_reset_postdata();
		?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Eof Testo-->
<?php } ?>

<?php if( $theme_options['home-org-block-status'] == true ) { ?>
<div class="clearfix">
  <div class="lightblue-break">
    <div class="container">
      <div class="row">
        <div class="margin-15">
          <div class="col-md-12 col-sm-12 margin-btm-20" style="text-align:center;">
            <h2 class="margin-btm-25"><?php echo $theme_options['home-org-block-main-title']; ?></h2>
            <p class="custom-size"><?php echo $theme_options['home-org-block-sub-title']; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-4" style="min-height: 516px; background: url(<?php if( isset($theme_options['home-org-block-background-url']['url']) && $theme_options['home-org-block-background-url']['url'] != '' ) { echo esc_url($theme_options['home-org-block-background-url']['url']); } ?>) 50% 50% / cover no-repeat;"></div>
<div class="col-md-8" style="min-height: 516px;">
  <div class="row">
    <?php
            $args = array(
                        'post_type' => 'manual_org_block',
                        'posts_per_page' => '-1',
                        'orderby' => 'menu_order',
						'post_status' => 'publish',
                        'order' => 'ASC'
                    );
            $wp_query = new WP_Query($args);
            if($wp_query->have_posts()) {
                $i = 0;
                while($wp_query->have_posts()) { $wp_query->the_post();
                if( $i % 2 == 0 ) $css = 'browse-whyus-desk';
                else $css = 'browse-seprate';
            ?>
    <div class="col-md-4 col-sm-6 org-box">
      <div style="text-align:center">
        <div class="home-box">
        <?php
		$icon_image_url = get_post_meta( get_the_ID(), '_manual_hpblock_custom_icon', 1 );
		if( $icon_image_url != '' ) {
			echo '<img src="'.$icon_image_url.'">';
		} else {
		?>
        <i class="<?php echo get_post_meta( $post->ID, '_manual_hpblock_icon', true ); ?> i-fa" style="font-size: 56px; color:#717171"></i>
        <?php } ?>
        </div>
        <h5>
          <?php the_title(); ?>
        </h5>
        <p><?php echo get_post_meta( $post->ID, '_manual_hpblock_text', true ); ?></p>
      </div>
    </div>
    <?php
                $i++; }
            }
            wp_reset_postdata();
            ?>
  </div>
</div>
</div>
<?php } ?>
<div class="clearfix"></div>

<?php if( $theme_options['de-message-bar'] == true ) { ?>
<div class="clearfix">
  <div class="lightblue-break">
    <div class="container padding-top-70-btm-70">
      <div class="row">
        <div class="margin-15">
          <div class="col-md-12 col-sm-12 margin-btm-20" style="text-align:center;">
            <h2 class="margin-btm-25"><?php echo $theme_options['message-bar-main-title']; ?></h2>
            <p class="custom-size"><?php echo $theme_options['message-bar-sub-title']; ?></p>
          </div>
          <div class="col-md-12 col-sm-12 message-bar-trim">
            <p class="home-message-darkblue-bar"> <a class="link hvr-icon-wobble-horizontal" href="<?php if( isset($theme_options['message-bar-bottom-url']) && $theme_options['message-bar-bottom-url']!= '' ){ echo $theme_options['message-bar-bottom-url']; } ?>" title=""><?php echo $theme_options['message-bar-bottom-display-text']; ?></a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<!--Fun Act-->
<?php if( $theme_options['home-funact-status'] == true ) { ?>
<div class="homepg-seprator-bg parallax-window" <?php if( isset($theme_options['funact-plx-url']['url']) && $theme_options['funact-plx-url']['url'] != '' ) { ?> data-image-src="<?php echo esc_url($theme_options['funact-plx-url']['url']); ?>" data-parallax="scroll" <?php } ?>>
  <div class="padding-top-70-btm-70 lightblue-break" <?php if( isset($theme_options['funact-plx-url']['url']) && $theme_options['funact-plx-url']['url'] != '' ) { ?> style="background: rgba(55, 56, 53, 0.5);" <?php } ?>>
    <div class="funact-main-div">
      <div class="container content-wrapper">
        <div class="row">
          <div class="col-md-12 col-sm-12 margin-btm-20" style="text-align:center;">
            <h2  class="margin-btm-25 <?php if( isset($theme_options['funact-plx-url']['url']) && $theme_options['funact-plx-url']['url'] != '' ) { ?> text-white-color <?php } ?>"><?php echo $theme_options['home-funact-main-title']; ?></h2>
            <p class="custom-size <?php if( isset($theme_options['funact-plx-url']['url']) && $theme_options['funact-plx-url']['url'] != '' ) { ?> text-white-color <?php } ?>"><?php echo $theme_options['home-funact-sub-title']; ?></p>
          </div>
          <div class="col-md-12 col-sm-12">
            <?php
	  foreach( $theme_options['home-funact-sortable'] as $key=>$val ) {
		 $funact[$theme_options['home-funact-no-sortable'][$key]] = $val;
	  }

	  if( count($funact) > 1 ) {
	  foreach( $funact as $key=>$val ) { ?>
            <div class="col-md-3 col-sm-6" style=" padding: 25px 3px;text-align: center;">
              <p><b class="timer counter-number funact counter-number-color <?php if( isset($theme_options['funact-plx-url']['url']) && $theme_options['funact-plx-url']['url'] != '' ) { ?> text-white-color <?php } ?>"  data-to="<?php echo $key ?>" data-speed="10000" ></b></p>
              <p class="counter-text countdown <?php if( isset($theme_options['funact-plx-url']['url']) && $theme_options['funact-plx-url']['url'] != '' ) { ?> text-white-color <?php } ?>"><?php echo $val; ?></p>
            </div>
            <?php } } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<!-- SECTION DIVISION -->
<!--
<div id="footer-info">
  <div class="bg-color home_banner">
    <div class="container">
      <div class="row" style="margin:1px 0px">
      	<div class="col-md-4 footer-msg-bar">
          <p>Você já conhece o Programa Nacional de Saneamento Rural? </p>
        </div>
        <div class="col-md-8 footer-msg-bar">
          <img src="./wp-content/uploads/2018/01/desenho.png">
        </div>
      </div>
    </div>
  </div>
</div>
-->
<!-- SECTION DIVISION -->

<?php
  // turn this section off by this momment
  if (TRUE == FALSE): 
?>

<div class="col-md-12 col-sm-12 margin-btm-20" style="text-align:center; margin-top: 40px !important">
    <h2 class="margin-btm-25 uppercase">Artigos</h2>
</div>

<?php



global $theme_options;



if( isset($theme_options['kb-cat-display-order']) && $theme_options['kb-cat-display-order'] != ''  ) {
	if( $theme_options['kb-cat-display-order'] == 1 ) {
		$cat_display_order = 'ASC';
	} else {
		$cat_display_order = 'DESC';
	}
}

if( isset($theme_options['kb-cat-display-order-by']) && $theme_options['kb-cat-display-order-by'] != ''  ) {
	$cat_display_order_by = $theme_options['kb-cat-display-order-by'];
} else {
	$cat_display_order_by = 'name';
}

// pages
if( isset($theme_options['kb-cat-page-display-order']) && $theme_options['kb-cat-page-display-order'] != ''  ) {
	if( $theme_options['kb-cat-page-display-order'] == 1 ) {
		$page_display_order = 'ASC';
	} else {
		$page_display_order = 'DESC';
	}
}
if( isset( $theme_options['kb-cat-page-display-order-by'] ) && $theme_options['kb-cat-page-display-order-by'] != '' ) {
	$display_page_order_by = $theme_options['kb-cat-page-display-order-by'];
} else {
	$display_page_order_by = 'date';
}
// eof page order
$id = get_the_ID();
$get_id = update_option('manual_breadcrumb_kb', $id);

//list terms in a given taxonomy
$args = array(
    'hide_empty'    => 1,
	'child_of' 		=> 0,
	'pad_counts' 	=> 1,
	'hierarchical'	=> 1,
	'order'         => $cat_display_order,
	'orderby'       => $cat_display_order_by,
);
$tax_terms = get_terms('manualknowledgebasecat', $args);

if( $theme_options['kb-home-page-allow-child'] == false ) $tax_terms = wp_list_filter($tax_terms,array('parent'=>0));
$col_md = 4;
$col_sm = 12;
?>

<!-- /start container -->
<div class="container content-wrapper body-content" style="margin-top: 80px;">
<div class="row  margin-top-btm-50 fix-margin-50">
<div class="masonry-grid">
  <?php
  $i = 1;
  foreach ($tax_terms as $tax_term) {
	$check_if_login_call = get_option( 'kb_cat_check_login_'.$tax_term->term_id );
	$check_user_role = get_option( 'kb_cat_user_role_'.$tax_term->term_id );
	$custom_login_message = get_option( 'kb_cat_login_message_'.$tax_term->term_id );
  ?>
  <div class="col-md-<?php echo $col_md; ?> col-sm-<?php echo $col_sm; ?> masonry-item">
    <!--Start-->
    <div class="knowledgebase-body">
      <h5><a href="<?php  echo esc_attr(get_term_link($tax_term, 'manualknowledgebasecat')); ?>">
            <?php
			 $cat_title = $tax_term->name;
			 echo $cat_title = html_entity_decode($cat_title, ENT_QUOTES, "UTF-8");
			?>
        </a> </h5>
      <span class="separator small"></span>

      <?php
	   if( $check_if_login_call == 1 && !is_user_logged_in() ) {
		 echo manual_get_login_form_control($custom_login_message);
	   } else {

	    if( !empty($check_user_role) ) $access_status = manual_doc_access(($check_user_role));
		else  $access_status = true;

		if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) {
				echo '<div class="manual_login_page"> <div class="custom_login_form"><p>';
				echo $theme_options['kb-cat-page-access-control-message'];
				echo '</p></div></div>';
		} else {
		// eof post access control

	   ?>
      <ul class="kbse">
        <?php
			if( isset( $theme_options['kb-no-of-records-per-cat'] ) && $theme_options['kb-no-of-records-per-cat'] != '' ) {
				$display_on_of_records_under_cat = $theme_options['kb-no-of-records-per-cat'];
			} else {
				$display_on_of_records_under_cat = '5';
			}

			$args = array(
				'post_type'  => 'manual_kb',
				'posts_per_page' => $display_on_of_records_under_cat,
				'orderby' => $display_page_order_by,
				'order'  => $page_display_order,
				'tax_query' => array(
					array(
						'taxonomy' => 'manualknowledgebasecat',
						'field' => 'term_id',
						'include_children' => true,
						'terms' => $tax_term->term_id
					)
				)
			 );
			 $st_cat_posts = get_posts( $args );
			 foreach( $st_cat_posts as $post ) : ?>
        <li class="cat inner"> <a href="<?php the_permalink(); ?>">
          <?php
			 $org_title = get_the_title();
			 echo $title = html_entity_decode($org_title, ENT_QUOTES, "UTF-8");
		  ?>
          </a> </li>
        <?php endforeach; ?>
      </ul>
      <div style="padding:10px 0px;">
      	<a href="<?php  echo esc_attr(get_term_link($tax_term, 'manualknowledgebasecat')); ?>" class="custom-link hvr-icon-wobble-horizontal kblnk" >
        <?php echo $theme_options['kb-cat-view-all']; ?>
        <?php echo $tax_term->count; ?> </a>
      </div>
      <?php }} ?>
    </div>
    <!--Eof Start class="custom-link"-->
  </div>
  <?php $i++; } wp_reset_postdata(); ?>
</div>

<?php endif //true == false ?>
<?php get_footer(); ?>
