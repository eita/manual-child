<?php

function INFOPNSR_the_plot_data_municipio ( $codmun ) {
  require ('config.php');

  echo INFOPNSR_c3_pie( $codmun, 10, $chart_content[10]['title'], $chart_content[10]['source'], $chart_content[10]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 14, $chart_content[14]['title'], $chart_content[14]['source'], $chart_content[14]['uuid']);
  echo INFOPNSR_c3_pie( $codmun, 21, $chart_content[21]['title'], $chart_content[21]['source'], $chart_content[21]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 26, $chart_content[26]['title'], $chart_content[26]['source'], $chart_content[26]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 27, $chart_content[27]['title'], $chart_content[27]['source'], $chart_content[27]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 32, $chart_content[32]['title'], $chart_content[32]['source'], $chart_content[32]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 31, $chart_content[31]['title'], $chart_content[31]['source'], $chart_content[31]['uuid']);
  echo INFOPNSR_c3_line( $codmun, 94, $chart_content[94]['title'], $chart_content[94]['source'], $chart_content[94]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 304, $chart_content[304]['title'], $chart_content[304]['source'], $chart_content[304]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 215, $chart_content[215]['title'], $chart_content[215]['source'], $chart_content[215]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 214, $chart_content[214]['title'], $chart_content[214]['source'], $chart_content[214]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 305, $chart_content[305]['title'], $chart_content[305]['source'], $chart_content[305]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 216, $chart_content[216]['title'], $chart_content[216]['source'], $chart_content[216]['uuid']);
  echo INFOPNSR_c3_stacked_100_percent( $codmun, 303, $chart_content[303]['title'], $chart_content[303]['source'], $chart_content[303]['uuid']);
}

function INFOPNSR_the_plot_data_uf ( $coduf ) {
  echo INFOPNSR_c3_pie( $coduf, 39, "Domicílio possui filtro em casa?", array('pnad_2015'));
}


function INFOPNSR_number ( $codmun, $question, $title, $source, $noresults, $uuid ) {
  $answer = INFOPNSR_metabase_get_answer( $uuid, $codmun );
  $first = true;

  foreach ($answer[0] as $key => $value) {
    // $title = $key;
    $number = $value;
  }

  $return = '<h1 style="margin-left: 30px;padding-top: 10px;" class="INFOPNSR_chart_title">' . $title . '</h1>';
  if ($number === NULL) {
    $return .= "<p>$noresults</p>";
  } else {
    $return .= '<p class="INFOPNSR_number">' . $value . '</p>';
  }

  $return .= INFOPNSR_get_the_source( $source );
  return $return;
}

function INFOPNSR_c3_line ( $codmun, $question, $title, $source, $uuid ) {
  $answer = INFOPNSR_metabase_get_answer( $uuid, $codmun );

  foreach ($answer as $row) {
    $first = true;
    foreach ($row as $key => $value) {
      if ($first) {
        $columns[] = $value;
        $first = false;
        $column_name = $key;
      } else {
        $ticks[] = $value;
        $tick_name = $key;
      }
    }
  }

  $return = "
    document.getElementById('plot$question').outerHTML += '" . INFOPNSR_get_the_source( $source ) . "';
  ";

  $return .= "
  (function($){
    $('.panel-collapse.collapse').on('shown.bs.collapse', function () {
      var chart_width = document.getElementById('plot$question').clientWidth;
      var chart = c3.generate({
        bindto : '#plot$question',
        title: {
          text: '$title',
          position: 'top-center',
          padding: {top:0, bottom:20}
        },
        size: {
          width: chart_width,
          height: 320
        },
        data: {
          columns: [
            ['$column_name', " . implode(", ", $columns). "]
          ]
        },
        axis: {
          x: {
            type: 'category',
            tick: {
              fit: true,
              rotate: 90,
              multiline: false
            },
            categories: [ " . implode(", ", explode("*", "'" . implode("'*'", $ticks) . "'")) . "],
            label: {
              text: '$tick_name',
              position: 'outer-center'
            }
          },
          y: {
            label: {
              text: '$column_name',
              position: 'outer-middle'
            }
          }
        },
        legend: {
          show: false
        }
      })
      setTimeout(function() {svgtocanvas ( $question );},2000);
    })
  })(jQuery);
  ";
  return "<script>" . $return . "</script>";
}
function INFOPNSR_table ( $codmun, $question, $title, $source, $noresults, $uuid ) {
  $answer = INFOPNSR_metabase_get_answer( $uuid, $codmun );
  $first = true;
  $return = '<h1 class="INFOPNSR_chart_title" style="margin-left: 30px;padding-top: 10px;">' . $title . '</h1>';
  if (!$answer) {
    $return .= "<p>$noresults</p>";
  } else {
    $return .= "<table>";
    for ($i=0; $i < count($answer); $i++) {
      $return .= "<tr>";
      foreach ($answer[$i] as $key => $value) {
        if ($first) {
          $return .= "<th>$key</th>";
        } else {
          $return .= "<td>$value</td>";
        }
      }
      $return .= "</tr>";
      if($first){
          $first = false;
          $i = -1;
      }
    }
    $return .= "</table>";
  }
  $return .= INFOPNSR_get_the_source( $source );
  return $return;
}

function INFOPNSR_c3_pie( $codmun, $question, $title, $source, $uuid, $print = FALSE ) {

  $answer = INFOPNSR_metabase_get_answer( $uuid, $codmun );
  if (!is_array($answer)) {return "";}

  foreach ($answer as $a) {
    foreach ($a as $key => $value) {
      if ($key != "classe") {
        $classes[$a->classe][$key] = $value;
        $columns[$key][$a->classe] = $value;
      }
    }
  }

  $return = "
    document.getElementById('plot$question').outerHTML += '" . INFOPNSR_get_the_source( $source ) . "';
  ";

  $return .= "
  (function($){
    $('.panel-collapse.collapse').on('shown.bs.collapse', function () {
    var chart_width = document.getElementById('plot$question').clientWidth;
    var chart = c3.generate({
        bindto : '#plot$question',
        title: {
          text: '$title',
          position: 'top-center',
          padding: {top:0, bottom:20}
        },
        size: {
          width: chart_width,
          height: 320
        },
        data: {
            columns: [";

  $print_columns = NULL;
  foreach ($classes as $key => $value) {
    $return .= "['" . $key . "', " . implode (", ", $value) . "],
      ";
    $print_columns[] = array_merge(array($key),array_values($value));
  }

  $return .= "],
            type : 'pie',
        },
        pie: {
          label: {
              format: function (value, ratio, id) {
                  return d3.format('.1%')(ratio);
              }
          }
        },
        tooltip: {
            format: {
                title: function (d) { return 'Valor absoluto'; },
                value: function (value, ratio, id) {
                    return value;
                }
            }
        }
      });
      setTimeout(function() {svgtocanvas ( $question );},2000);
    });
  })(jQuery);
  ";

  return "<script>" . $return . "</script>";
}

function INFOPNSR_c3_stacked_100_percent( $codmun, $question, $title, $source, $uuid ) {

  $answer = INFOPNSR_metabase_get_answer( $uuid , $codmun );
  foreach ($answer as $a) {
    foreach ($a as $key => $value) {
      if ($key != "classe") {
        $classes[$a->classe][$key] = $value;
        $columns[$key][$a->classe] = $value;
      } else {
        $keys[] = $value;
      }
    }
  }

  foreach ($classes as $nome => $classe) {
    $sum[$nome] = array_sum ( $classe );
  }

  $return = "
    document.getElementById('plot$question').outerHTML += '" . INFOPNSR_get_the_source( $source ) . "';
  ";

  $return .= "
  (function($){
    $('.panel-collapse.collapse').on('shown.bs.collapse', function () {
      var chart_width = document.getElementById('plot$question').clientWidth;
      var chart = c3.generate({
          bindto : '#plot$question',
          title: {
            text: '$title',
            position: 'top-center',
            padding: {top:0, bottom:20}
          },
          size: {
            width: chart_width,
            height: 320
          },
          data: {
              order: 'desc',
              columns: [
      ";
  $first = true;
  foreach ($columns as $key => $value) {
    foreach ($value as $k => &$v) {
      $v = round($v / $sum[$k] * 100) / 100;
      if ($first){
        $x = array_search($k, $keys);
        $keys[$x] .= " ($sum[$k])";
      }
    }
    $first = false;
    $return .= "['" . $key . "', " . implode (", ", $value) . "],
    ";
  }
  $return .= "
              ],
              type: 'bar',
              groups: [
                  [" . implode(", ", explode("*", "'" . implode("'*'", array_keys($columns)) . "'")) . "]
              ]
          },
          grid: {
              y: {
                  lines: [{value:0}]
              }
          },
          axis: {
              x: {
                  type: 'category',
                  categories: [" . implode(", ", explode("*", "'" . implode("'*'", $keys) . "'")) . "]
              },
              y: {
                  max: 1,
                  min: 0,
                  padding: {top:0, bottom:0},
                  tick: {
                    format: d3.format('%')
                  }
              }
          },
          legend: {
            show: false
          },
          onrendered: function() {
          	$('#plot$question').css('max-height', 'none');
          }
      });
      window.createLegend(chart, '#plot$question');
      setTimeout(function() {svgtocanvas ( $question );},2000);
    });
  })(jQuery);
  ";

  return "<script>(function($){" . $return . "})(jQuery);</script>";
}

function INFOPNSR_get_the_source ( $sources ) {

  $return = NULL;

  if (!is_array($sources)) {return "";}

  foreach ($sources as $source) {
    $args = array(
      'post_type' => 'datasource',
      'meta_query' => array (
      	array(
      		'key'     => 'id',
      		'value'   => $source,
      		'compare' => '='
      	)
      )
    );
    $query_src = new WP_Query( $args );

    while( $query_src->have_posts()) {
      $query_src->the_post();
      $return[] = '<a href="' . get_the_permalink( $query_src->post->ID ) . '">' . get_the_title( $query_src->post->ID ) . '</a>';
    }
    wp_reset_postdata();
  }

  return '<p class="INFOPNSR_fonte">Fonte: ' . implode( "/", $return ) . '</p>';
}
