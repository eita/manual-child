<?php

$GLOBALS['metabase_url'] = "http://localhost:3000";
// $GLOBALS['metabase_url'] = "https://metabase.eita.coop.br";

$chart_content = array(
  10 => array(
    'title' => 'População Urbana e Rural (2010)',
    'text' => 'Neste gráfico, podemos ver o percentual da população do município que vive em zonas consideradas urbanas e zonas consideradas rurais. No contexto do PNSR, a definição de zonas rurais e urbanas original do IBGE foi modificada de modo a expressar melhor a realidade em cada município.',
    'type' => 'pie',
    'source' => array('censo_2010','pnsr_rural'),
    'uuid' => '89d56a68-bc1a-4197-89f7-3684f01c21e1'
  ),
  // 21 => array(
  //   'title' => "Indígenas nas Zonas Rural e Urbana (2010)",
  //   'text' => 'Neste gráfico, podemos ver o percentual da população que se auto-declara indígena vivendo nas zonas classificadas como urbanas e nas zonas classificadas como rurais.',
  //   'type' => 'pie',
  //   'source' => array('censo_2010','pnsr_rural'),
  //   'uuid' => 'f2d26c63-4012-4b7d-9dac-8e4fab093fe2'
  // ),
  14 => array(
    'title' => "Raça e Cor nas Zonas Rural e Urbana (2010)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% da população rural e outra representando 100% da população urbana. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de raça/cor definidas pelo IBGE. A classificação é auto-declarada. O gráfico exibe a a distribuição da população por raça e cor nas zonas consideradas urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censo_2010','pnsr_rural'),
    'uuid' => '2ddc26d5-ed9a-4892-bc3e-8d500dc01fc4'
  ),
  26 => array(
    'title' => "Abastecimento de Água (2010)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% dos domicílios rurais e outra representando 100% dos domicílios urbanos. O total de domicílios em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de abastecimento de água definidas pelo IBGE. O gráfico exibe a a distribuição das formas de abastecimento de água nas zonas consideradas urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censo_2010','pnsr_rural'),
    'uuid' => 'f5da3d0f-3304-4e55-a318-c35514a428e0'
  ),
  27 => array(
    'title' => "Esgotamento Sanitário (2010)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% dos domicílios rurais e outra representando 100% dos domicílios urbanos. O total de domicílios em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de esgotamento sanitário definidas pelo IBGE. O gráfico exibe a a distribuição das formas de esgotamento sanitário nas zonas consideradas urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censo_2010','pnsr_rural'),
    'uuid' => 'b57d758d-529b-41f1-8643-dd7f18fb306b'
  ),
  32 => array(
    'title' => "Domicílios Sem Banheiro (2010)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% dos domicílios rurais e outra representando 100% dos domicílios urbanos. O total de domicílios em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de domicílios sem banheiro. O gráfico exibe a a distribuição de domicílio sem banheiro nas zonas consideradas urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censo_2010','pnsr_rural'),
    'uuid' => '8a1d705f-a951-491f-b259-ad2a5ff71182'
  ),
  31 => array(
    'title' => "Destinação do Lixo (2010)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% dos domicílios rurais e outra representando 100% dos domicílios urbanos. O total de domicílios em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de destinação de lixo definidas pelo IBGE. O gráfico exibe a distribuição das formas de destinação do lixo nas zonas consideradas urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censo_2010','pnsr_rural'),
    'uuid' => 'b38f3b9a-b431-4ade-a745-200e4a23cfd6'
  ),
  22 => array(
    'title' => "Comunidades Quilombolas Reconhecidas (até 2017)",
    'text' => 'A Fundação Cultural Palmares é o órgão brasileiro responsável por homologar as terras quilombolas. No entanto, diversas comunidades quilombolas ainda lutam pelo direito de ter suas terras oficialmente demarcadas.',
    'type' => 'table',
    'source' => array('fcp'),
    'noresults' => "Este município não possui nenhuma comunidade quilombola demarcada. Isso não significa, no entanto, que não haja terras quilombolas não demarcadas.",
    'uuid' => 'ef176a74-ed9e-4b3a-8743-739b2987c04f'
  ),
  9 => array(
    'title' => "População em Terras Indígenas (2010)",
    'text' => 'A Funai é o órgão brasileiro responsável pela demarcação oficial de terras indígenas. No entanto, diversos territórios são reivindicados por indígenas, mas ainda não foram demarcados oficialmente.',
    'type' => 'number',
    'source' => array('censo_2010'),
    'noresults' => "Este município não possui nenhuma terra indígena demarcada pela Funai. Isso não significa, no entanto, que não haja terras indígenas não-demarcadas.",
    'uuid' => 'f047fc85-a7b3-4846-8798-d501856658f4'
  ),
  94 => array(
    'title' => "Taxa de Mortalidade Infantil (1996 a 2017)",
    'text' => 'A taxa de mortalidade infantil representa o número de óbitos infantis (crianças que morrem antes de completar um ano) para cada 1000 nascidos vivos.',
    'type' => 'line',
    'source' => array('datasus', 'sinasc'),
    'uuid' => '8d00cfa1-3d3b-481b-883a-670de5d9645b'
  ),
  304 => array(
    'title' => "Fornece água potável para o consumo humano? (2019)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% das escolas rurais e outra representando 100% das escolas urbanas. O total de escolas em cada zona está descrito abaixo da barra. As cores de cada segmento da barra mostram o percentual de escolas que fornece água potável para os estudantes.',
    'type' => 'bar_stacked_100',
    'source' => array('censoescolar_2019'),
    'uuid' => 'ce24299d-2176-4816-be36-2fc259f08a92'
  ),
  305 => array(
    'title' => "Possui banheiro na escola? (2019)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% das escolas rurais e outra representando 100% das escolas urbanas. O total de escolas em cada zona está descrito abaixo da barra. As cores de cada segmento da barra mostram o percentual de escolas que possui banheiro.',
    'type' => 'bar_stacked_100',
    'source' => array('censoescolar_2019'),
    'uuid' => '5dcb926d-a4ff-45b2-81b1-ae8aa7bc30ed'
  ),
  215 => array(
    'title' => "Abastecimento de água nas escolas (2019)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% das escolas rurais e outra representando 100% das escolas urbanas. O total de escolas em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de abastecimento de água nas escolas definidas pelo Censo Escolar. O gráfico exibe a distribuição das formas de abastecimento nas escolas consideradas como urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censoescolar_2019'),
    'uuid' => 'ca129e92-e6a4-4a69-8aef-efc71cd9ef59'
  ),
  214 => array(
    'title' => "Esgotamento sanitário nas escolas (2019)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% das escolas rurais e outra representando 100% das escolas urbanas. O total de escolas em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de esgotamento sanitário nas escolas definidas pelo Censo Escolar. O gráfico exibe a distribuição das formas de esgotamento sanitário nas escolas consideradas como urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censoescolar_2019'),
    'uuid' => '3ad77085-a532-42e6-a338-97f880c70ee3'
  ),
  216 => array(
    'title' => "Destinação do lixo nas escolas (2019)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% das escolas rurais e outra representando 100% das escolas urbanas. O total de escolas em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de destinação de lixo nas escolas definidas pelo Censo Escolar. O gráfico exibe a distribuição das formas de destinação e tratamento do lixo nas escolas consideradas como urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censoescolar_2019'),
    'uuid' => '597b4a02-4cd3-4ea9-a2b8-94e969c1a9e2'
  ),
  303 => array(
    'title' => "Tratamento do lixo nas escolas (2019)",
    'text' => 'Este gráfico exibe duas barras coloridas, uma representando 100% das escolas rurais e outra representando 100% das escolas urbanas. O total de escolas em cada zona está descrito abaixo da barra. A cores de cada segmento da barra mostram o percentual de cada uma das classificações de destinação de lixo nas escolas definidas pelo Censo Escolar. O gráfico exibe a distribuição das formas de tratamento do lixo nas escolas consideradas como urbanas e rurais.',
    'type' => 'bar_stacked_100',
    'source' => array('censoescolar_2019'),
    'uuid' => 'ba3d4c60-a6ca-45f2-b467-b4291550d3dc'
  ),
  328 => array(
    'title' => "Agrotóxicos detectados na água (2020)",
    'text' => 'A lista exibe os agrotóxicos detectados nas análises de água realizadas no município, ou seja, as análises cujo resultado foi um valor numérico ou abaixo do limite de quantificação.',
    'type' => 'table',
    'source' => array('sisagua_2020'),
    'uuid' => 'fb17943e-9a0a-4acf-a99a-950f7464261c',
    'noresults' => "Este município não apresentou nenhum agrotóxico detectado. Isso não significa, no entanto, que não haja agrotóxicos na água. Verifique o número de análises realizadas.",
  ),
  330 => array(
    'title' => "Número de análises por grupo de parâmetros (2020)",
    'text' => 'A lista exibe o número de análises de água realizadas em 2020, por grupo de parâmetros.',
    'type' => 'table',
    'source' => array('sisagua_2020'),
    'uuid' => '93c214f3-7810-4f9c-87b8-fe2226b31e27',
    'noresults' => "Este município não apresentou nenhuma análise de água em 2020.",
  ),
);

$UUID = array(
  28 => 'ae8b7d28-0c2d-4236-beb1-15d1e8c08bc1',
  47 => 'f958c6be-6770-49a4-8d6c-9d0c89a6955e',
  48 => '8cc859fa-12ad-4f40-96bf-9b06f658ab8f',
  49 => '3c65eaaa-a6ff-4846-9a00-c06460ead610',
  11 => '40cc5d50-668b-40b8-8a25-d1384d92eabe',
  13 => '599d6fa0-20ba-4c1e-89da-e209e6492c45',
  19 => '4fa14a35-ae78-4950-a00e-01f2d0b188b0',
  29 => '1e779782-3e99-4851-8bb8-fe87e2e35c8f',
  30 => 'eb66cf67-b964-4dcb-837c-fd265e9fb008',
  17 => 'ad54a581-1679-47ce-bf6d-1a65cf7f89a8',
  18 => 'da3cb0e9-5374-412d-9435-4b3b5e072912'
);
