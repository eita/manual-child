(function($){
  $body = $("body");
  $body.addClass("loading");
  // console.log("1");
  $( document ).ready(function() {
    $body.removeClass("loading");
    // console.log("2");
  });

})(jQuery);

(function($){
  $('a').click(function () {
      $('#jquery-live-search').hide()
  })
})(jQuery);

(function($){
  $('#INFOPNSR_searchicon').click(function () {
      var input = $('.INFOPNSR_headersearch input[type="text"]');
      expanded = input.is(".expanded");
      if (expanded) {
          input.removeClass('expanded').animate({
              right: -1000
          })
      } else {
          input.animate({
              right: 40
          }, function () {
              input.addClass('expanded');
              $('.INFOPNSR_headersearch input[type="text"]').focus();
          })
      }
  })
  $('.INFOPNSR_headersearch input[type="text"]').blur(function () {
    $('.INFOPNSR_headersearch input[type="text"]').animate({
      right: -1000
    })
    setTimeout(function () {
      $('.INFOPNSR_headersearch input[type="text"]').removeClass('expanded');
    }, 2000);
  })
})(jQuery);

(function($){
  window.createLegend = function(chart, container) {
    var dataKeys = Object.keys(chart.internal.data.xs);
    var names = chart.data.names();
  	// console.log("createLegend", dataKeys, names, chart);

    $(container).css("max-height", "none");
    // $(container).css("width", chart.internal.currentWidth);

    d3.select(container).insert('div', '.chart').attr('class', 'legend').selectAll('div')
      .data(dataKeys)
      .enter().append('div')
      .attr('class', 'legend-item')
      .attr('data-id', function(id) {
        return id;
      })
      .each(function(id) {
        d3.select(this)
        .append('span').style('background-color', chart.color(id)).attr('class', 'legend-box');
        var text = names[id];
        if (!text) text = id;
        d3.select(this)
        .append('span').html(text).attr('class', 'legend-text');
      })
      .on('mouseover', function(id) {
        chart.focus(id);
      })
      .on('mouseout', function(id) {
        chart.revert();
      })
      .on('click', function(id) {
        $(this).toggleClass("c3-legend-item-hidden")
        chart.toggle(id);
        $(container).css("max-height", "none");
      });
  };
})(jQuery);

function svgtocanvas ( question ) {
  var css = '\
  <?xml-stylesheet type="text/css" href="/wp-content/themes/manual-child/style.css?ver=4.9.4" ?>\
  <?xml-stylesheet type="text/css" href="/wp-content/themes/manual-child/includes/c3.css?ver=0.4.18" ?>';
  // var original_svg = document.getElementById('plot' + question).firstChild;
  // original_svg.classList.add("c3");
  // original_svg.classList.add("chart");
  var svgString = new XMLSerializer().serializeToString(document.getElementById('plot' + question).firstChild);
  var canvas = document.getElementById('canvas' + question );
  var ctx = canvas.getContext('2d');
  var DOMURL = self.URL || self.webkitURL || self;
  var img = new Image();
  var svg = new Blob([css+svgString], {type: 'image/svg+xml;charset=utf-8'});
  var url = DOMURL.createObjectURL(svg);
  img.onload = function() {
      // console.log('onload');
      ctx.drawImage(img, 0, 0);
      var png = canvas.toDataURL('image/png');
      document.getElementById('png-container' + question).innerHTML = '<img src="'+png+'"/>';
      DOMURL.revokeObjectURL(png);
  };
  // console.log(url);
  img.src = url;
  var button = document.getElementById('imgdownload' + question);
  button.onclick = function () {
      window.location.href = img.src.replace('image/png', 'image/octet-stream');
  };
}
