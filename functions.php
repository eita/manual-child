<?php
require ( 'config.php' );
require ( 'functions-plot.php' );
require ( 'functions-metabase.php' );
require ( 'functions-template.php' );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_script( 'infopnsr', get_stylesheet_directory_uri() . '/js/infopnsr.js' , array('jquery'), '1.0.0', true );

    wp_enqueue_style( 'leaflet', get_stylesheet_directory_uri() . '/includes/leaflet.css' , NULL , '1.2.0' );
    wp_enqueue_script( 'leaflet', get_stylesheet_directory_uri() . '/includes/leaflet.js' , NULL , '1.2.0' );

    wp_enqueue_script( 'd3', get_stylesheet_directory_uri() . '/includes/d3.min.js' , NULL , '3.5.17');
    wp_enqueue_style( 'c3', get_stylesheet_directory_uri() . '/includes/c3.min.css' , NULL , '0.4.17' );
    wp_enqueue_script( 'c3', get_stylesheet_directory_uri() . '/includes/c3.min.js' , NULL , '0.4.17' );

}

function INFOPNSR_register_municipio() {
  $args = array(
  	'public' => true,
  	'label'  => 'Município',
      'has_archive' => true,
      'rewrite' => array('slug' => 'municipio'),
      'supports' => array(
      	'title',
  		'editor',
  		'thumbnail',
  		'comments',
  		'revisions',
  		'custom-fields'
  	),
  );
  register_post_type( 'municipio', $args );
}
add_action( 'init', 'INFOPNSR_register_municipio' );

function INFOPNSR_register_estado() {
  $args = array(
  	'public' => true,
  	'label'  => 'Estado',
      'has_archive' => true,
      'rewrite' => array('slug' => 'estado'),
      'supports' => array(
      	'title',
  		'editor',
  		'thumbnail',
  		'comments',
  		'revisions',
  		'custom-fields'
  	),
  );
  register_post_type( 'estado', $args );
}
add_action( 'init', 'INFOPNSR_register_estado' );

function INFOPNSR_register_datasource() {
  $args = array(
    'public' => true,
    'label'  => 'Fonte de Dados',
    'has_archive' => true,
    'rewrite' => array('slug' => 'fontededados'),
    'supports' => array(
    	'title',
    	'editor',
    	'thumbnail',
    	'comments',
    	'revisions',
    	'custom-fields'
    ),
    'taxonomies' => array('datasourcetax')
  );
  register_post_type( 'datasource', $args );
}
add_action( 'init', 'INFOPNSR_register_datasource' );

function INFOPNSR_register_datasourcetax() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Categorias de Fontes de Dados', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Categoria de Fontes de Dados', 'taxonomy singular name', 'textdomain' ),
		// 'search_items'      => __( 'Search Genres', 'textdomain' ),
		// 'all_items'         => __( 'All Genres', 'textdomain' ),
		// 'parent_item'       => __( 'Parent Genre', 'textdomain' ),
		// 'parent_item_colon' => __( 'Parent Genre:', 'textdomain' ),
		// 'edit_item'         => __( 'Edit Genre', 'textdomain' ),
		// 'update_item'       => __( 'Update Genre', 'textdomain' ),
		// 'add_new_item'      => __( 'Add New Genre', 'textdomain' ),
		// 'new_item_name'     => __( 'New Genre Name', 'textdomain' ),
		// 'menu_name'         => __( 'Genre', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'dados' ),
	);

	register_taxonomy( 'datasourcetax', array( 'datasource' ), $args );
}

add_action( 'init', 'INFOPNSR_register_datasourcetax', 0 );

function INFOPNSR_footer ( $cod ) {
  $time_pre = microtime(true);
  if (!wp_is_mobile()) {
  	echo "<script>"	. INFOPNSR_get_coordinates ( $cod ) . "</script>";
    echo "<script>"	. INFOPNSR_get_terras_indigenas ( $cod ) . "</script>";
    echo "<script>"	. INFOPNSR_get_quilombos ( $cod ) . "</script>";
    echo "<script>"	. INFOPNSR_get_assentamentos ( $cod ) . "</script>";
  }
  if ( 'municipio' == get_post_type() ) {
    INFOPNSR_the_plot_data_municipio ( $cod );
  } elseif ( 'estado' == get_post_type() ) {
    INFOPNSR_the_plot_data_uf ( $cod );
  }
  $time_post = microtime(true);
  $exec_time = $time_post - $time_pre;
  // echo "---Mapa: $exec_time---";

}

function INFOPNSR_generate_municipios () {

  $estados = array (
    'Acre' => 'AC',
    'Amapá' => 'AP',
    'Amazonas' => 'AM',
    'Roraima' => 'RR',
    'Rondônia' => 'RO',
    'Pará' => 'PA',
    'Maranhão' => 'MA',
    'Piauí' => 'PI',
    'Tocantins' => 'TO',
    'Ceará' => 'CE',
    'Rio Grande do Norte' => 'RN',
    'Paraíba' => 'PB',
    'Pernambuco' => 'PE',
    'Alagoas' => 'AL',
    'Sergipe' => 'SE',
    'Bahia' => 'BA',
    'Espírito Santo' => 'ES',
    'Rio de Janeiro' => 'RJ',
    'São Paulo' => 'SP',
    'Minas Gerais' => 'MG',
    'Paraná' => 'PR',
    'Santa Catarina' => 'SC',
    'Rio Grande do Sul' => 'RS',
    'Mato Grosso' => 'MT',
    'Mato Grosso do Sul' => 'MS',
    'Goiás' => 'GO',
    'Distrito Federal' => 'DF',
  );

	$municipios = INFOPNSR_metabase_get_answer(33);
	$i=0;
	foreach ($municipios as $municipio) {
		$postarr = array(
			'post_title' => INFOPNSR_municipio_case($municipio->Município) . ", " . $estados[$municipio->UFN],
			'post_status' => "publish",
			'post_type' => "municipio",
			'meta_input' => array( 'codmun' => $municipio->Codmun7 )
		);

		wp_insert_post ( $postarr );
		// if ($i > 100) exit;
		// $i++;
	}
}

function INFOPNSR_delete_municipios () {
  $args = array(
  	'post_type' => 'municipio',
    'posts_per_page' => -1
  );
  $i = 0;
  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
  	foreach ($query->posts as $post) {
  		wp_delete_post ($post->ID);
  	}
  }
}

function INFOPNSR_municipio_case ( $nome ){

  // use default php function
  $result = ucwords(strtolower($nome), '- ');

  // fix special chars
	$result = str_replace("Ã", "ã", $result);
	$result = str_replace("Õ", "õ", $result);
	$result = str_replace("Á", "á", $result);
	$result = str_replace("É", "é", $result);
  $result = str_replace("Ê", "ê", $result);
	$result = str_replace("Í", "í", $result);
	$result = str_replace("Ó", "ó", $result);
	$result = str_replace("Ú", "ú", $result);
	$result = str_replace("Â", "â", $result);
	$result = str_replace("Ẽ", "ê", $result);
	$result = str_replace("Ô", "ô", $result);
	$result = str_replace("Ç", "ç", $result);
	$result = str_replace("De ", "de ", $result);
	$result = str_replace("Do ", "do ", $result);
  $result = str_replace("Da ", "da ", $result);
  $result = str_replace("Dos ", "dos ", $result);
  $result = str_replace("Das ", "das ", $result);


  // fix special chars at the beginn of words
  $result = explode( " ", $result );
  $result2 = NULL;
  foreach($result as $res) {
    if (substr($res,0,2) == "á") $res = "Á" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "ã") $res = "Ã" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "â") $res = "Â" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "é") $res = "É" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "ê") $res = "Ê" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "í") $res = "Í" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "ó") $res = "Ó" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "ô") $res = "Ô" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "õ") $res = "Õ" . substr($res,2,strlen($res)-1);
    if (substr($res,0,2) == "ú") $res = "Ú" . substr($res,2,strlen($res)-1);
    $result2[] = $res;
  }

  $result = implode(" ", $result2);
  return $result;
}

function INFOPNSR_generate_estados () {

  $estados = array (
    'Acre' => array('AC',12),
    'Amapá' => array('AP',16),
    'Amazonas' => array('AM',13),
    'Roraima' => array('RR',14),
    'Rondônia' => array('RO',11),
    'Pará' => array('PA',15),
    'Piauí' => array('PI',22),
    'Maranhão' => array('MA',21),
    'Tocantins' => array('TO',17),
    'Ceará' => array('CE',23),
    'Rio Grande do Norte' => array('RN',24),
    'Paraíba' => array('PB',25),
    'Pernambuco' => array('PE',26),
    'Alagoas' => array('AL',27),
    'Sergipe' => array('SE',28),
    'Bahia' => array('BA',29),
    'Espírito Santo' => array('ES',32),
    'Rio de Janeiro' => array('RJ',33),
    'São Paulo' => array('SP',35),
    'Minas Gerais' => array('MG',31),
    'Paraná' => array('PR',41),
    'Santa Catarina' => array('SC',42),
    'Rio Grande do Sul' => array('RS',43),
    'Mato Grosso do Sul' => array('MS',50),
    'Mato Grosso' => array('MT',51),
    'Goiás' => array('GO',52),
    'Distrito Federal' => array('DF',53),
  );

	foreach ($estados as $estado => $sigla) {
		$postarr = array(
			'post_title' => $estado,
			'post_status' => "publish",
			'post_type' => "estado",
			'meta_input' => array( 'codes' => $sigla[1] , 'sigla' => $sigla[0])
		);

		wp_insert_post ( $postarr );
		// if ($i > 100) exit;
		// $i++;
	}
}

function INFOPNSR_searchbox () {
  ?>
  <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <i id="INFOPNSR_searchicon" class="fa fa-search livesearch"></i>
    <div class="form-group">
      <input type="text"  placeholder="<?php echo $theme_options['global-search-text-paceholder']; ?>" value="<?php echo get_search_query(); ?>" name="s" id="s" class="form-control header-search" />
      <!--Advance Search-->
      <?php
    $display_on_forum_page = '';
    if( $theme_options['manual-trending-post-type-search-status-on-forum-pages'] == false ) {
      if( get_post_type() == 'forum' ) $display_on_forum_page = 1;
    } else {
       $display_on_forum_page = 2;
    }
    if ( $theme_options['manual-trending-post-type-search-status'] == true && ($display_on_forum_page != 1) ){
      echo '<select class="search-expand-types" name="post_type" id="search_post_type">';
      echo ' <option value="">'.$theme_options['manual-post-type-search-text-inital'].'</option>';
      foreach ( $theme_options['manual-search-post-type-multi-select']  as $post_type ) {

        if( $post_type == 'manual_ourteam' ||
           $post_type == 'manual_tmal_block' ||
           $post_type == 'manual_org_block' ||
           $post_type == 'manual_hp_block'  ||
           $post_type == 'reply' ||
           $post_type == 'topic' ) { continue; }

        if( $post_type == 'attachment' ) { $new_name = $theme_options['manual-post-type-search-dropdown-media'];
        } else if( $post_type == 'forum' ) { $new_name = $theme_options['manual-post-type-search-dropdown-forums'];
        } else if( $post_type == 'manual_kb' ) { $new_name = $theme_options['manual-post-type-search-dropdown-kb'];
        } else if( $post_type == 'manual_faq' ) { $new_name = $theme_options['manual-post-type-search-dropdown-faq'];
        } else if( $post_type == 'manual_portfolio' ) { $new_name = $theme_options['manual-post-type-search-dropdown-portfolio'];
        } else if( $post_type == 'manual_documentation' ) { $new_name = $theme_options['manual-post-type-search-dropdown-documentation'];
        } else if( $post_type == 'page' ) { $new_name = $theme_options['manual-post-type-search-dropdown-page'];
        } else if( $post_type == 'post' ) { $new_name = $theme_options['manual-post-type-search-dropdown-post'];
        } else {
          $post_type_label = get_post_type_object( $post_type );
          $new_name = $post_type_label->label;
        }


        if( isset($_GET['post_type']) && $_GET['post_type'] == $post_type ) $select = 'selected';
        else $select = '';

        echo ' <option '.$select.' value="'. $post_type .'">' . $new_name . '</option>';

      }
      echo ' </select>';
    } else {
      echo '<input type="hidden" value="" name="post_type" id="search_post_type">';
    }

    // translation search url fix
    $ajax_live_search_url = home_url('/');
    $ajax_live_search_url_query_str = parse_url($ajax_live_search_url, PHP_URL_QUERY);
    if($ajax_live_search_url_query_str != '' ) {
      parse_str($ajax_live_search_url_query_str, $url_result);
      foreach( $url_result as $key=>$val ) {
        echo '<input type="hidden" value="'.$val.'" name="'.$key.'" id="search_post_type">';
      }
    }
    ?>
    </div>
  </form>
  <?php

}

add_filter('og_image_init', 'my_og_image_init');
function my_og_image_init($images)
{
    if ( empty($images) ) {
        $images[] = 'https://infosanbas.org.br/wp-content/uploads/2018/04/marca_infosanbas.png';
    }
    return $images;
}
