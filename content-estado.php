<?php
$format = get_post_format();
global $theme_options;
?>

<div <?php post_class('blog'); ?> id="post-<?php the_ID(); ?>">

  <?php
  if( ($theme_options['blog_featured_image_on_the_header'] == false) && is_single() ) {
  	manual_post_thumbnail();
  } else if( !is_single() ) {
  	manual_post_thumbnail();
  }
  ?>
  <div class="blog-box-post-inner">
    <div class="entry-content clearfix">
      <?php
			if ( is_single() ) :
        the_estado_template ( get_post_meta(get_the_ID())['codes'][0] );
				the_content();
			else :
				the_excerpt();
			endif;

		?>
    </div>
    <?php
	if( is_single() ) {

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'manual' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'manual' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
	}
?>
    <?php if( is_single() ) { the_tags( '</span> <div class="tagcloud singlepgtag clearfix margin-btm-20 singlepg"><span><i class="fa fa-tags"></i> '.esc_html__( 'Tags:', 'manual' ).'</span>', '', '</div>' );  } ?>
    <?php if( is_single() ) {  if( $theme_options['blog_single_social_share_status'] == true ) { manual_social_share(get_permalink()); } } ?>
    <?php if ( !is_single() && !is_search() ) : ?>
    <?php if( $format != 'quote' ) { ?>
    <p> <a href="<?php echo esc_url( get_permalink() ); ?>" class="custom-link-blog hvr-icon-wobble-horizontal">
      <?php esc_html_e( 'Continue Reading', 'manual' ) ?>
      </a> </p>
    <?php } ?>
    <?php endif; ?>
  </div>
</div>
<?php
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
?>
