<?php

function INFOPNSR_metabase_get_answer ( $uuid, $codmun = NULL, $export_format = 'json') {

	if ($codmun) {
		$parameters = array(
		    "type" => "number",
		    "target" => array(
		        "variable",
		        array("template-tag",
		        "municipio")
		     ),
		    "value" => $codmun
		);
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $GLOBALS['metabase_url'] . "/api/public/card/" . $uuid . "/query/" . $export_format . "?parameters=" . urlencode("[".json_encode($parameters) . "]"));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Accept: application/json', 'X-Metabase-Session: ' . $_COOKIE['x_metabase_session']));
	// curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	$res = json_decode($res);

	return ($res);

}

function INFOPNSR_metabase_get_csv ( $uuid, $codmun = NULL, $export_format = 'csv') {

	if ($codmun) {
		$parameters = array(
		    "type" => "number",
		    "target" => array(
		        "variable",
		        array("template-tag",
		        "municipio")
		     ),
		    "value" => $codmun
		);
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $GLOBALS['metabase_url'] . "/api/public/card/" . $uuid . "/query/" . $export_format . "?parameters=" . urlencode("[".json_encode($parameters) . "]"));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/csv', 'X-Metabase-Session: ' . $_COOKIE['x_metabase_session']));
	// curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);

	$fid = fopen('data.csv','w');
	fwrite($fid,$res);
	fclose($fid);
	header('Content-Type: application/csv');
	header('Content-Disposition: attachment; filename=data.csv');
	header('Pragma: no-cache');
	readfile("data.csv");
	// exit;

	return ($res);

}

function INFOPNSR_get_coordinates ( $codmun ) {
	require ('config.php');
	$coordinates = INFOPNSR_metabase_get_answer($UUID[28],$codmun);

	$js = "var geojsonFeature = [";
	foreach ($coordinates as $coordinate) {
		$coordinate_dec = json_decode($coordinate->Geojson);
		if (strtolower($coordinate_dec->properties->TIPO) != strtolower($coordinate->classe)){
			$coordinate_dec->properties->TIPO = strtoupper($coordinate->classe);
			$coordinate->Geojson = json_encode($coordinate_dec);
		}
		$js .= $coordinate->Geojson . ",";
	}
	$js .= "]";

	$js .= "
		var mymap = L.map('mapid',{ scrollWheelZoom:false });

		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
				'<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>'
		}).addTo(mymap);

		L.geoJSON(geojsonFeature,
			{
			    style: function (feature) {
			    	if (feature.properties.TIPO == 'URBANO')
			        	return {color: 'grey',weight: 1};
			        else
			        	return {color: 'green',weight: 1};
			    }
			}).bindPopup(function (layer) {
			    return 'Código do Setor Censitário:' + layer.feature.properties.CD_GEOCODI
					+ ((!!layer.feature.properties.NM_DISTRIT) ? '<br />Nome do Distrito: ' + layer.feature.properties.NM_DISTRIT : '')
					+ ((!!layer.feature.properties.NM_SUBDIST) ? '<br />Nome do Subdistrito: ' + layer.feature.properties.NM_SUBDIST : '')
					+ ((!!layer.feature.properties.NM_BAIRRO) ? '<br />Nome do Bairro: ' + layer.feature.properties.NM_BAIRRO : '')
					+ ((!!layer.feature.properties.TIPO) ? '<br />Tipo do Setor: ' + layer.feature.properties.TIPO : '')
			}
		).addTo(mymap);
		mymap.fitBounds(L.geoJSON(geojsonFeature).getBounds());
	";
	return $js;
}

function INFOPNSR_get_terras_indigenas ( $codmun ) {
	require ('config.php');
	$coordinates = INFOPNSR_metabase_get_answer($UUID[47],$codmun);

	$js = "var geojsonFeature = [";
	foreach ($coordinates as $coordinate) {
		$coordinate_dec = json_decode($coordinate->geojson);
		$js .= $coordinate->geojson . ",";
	}
	$js .= "]";

	$js .= "

		var TI = L.geoJSON(geojsonFeature,
			{
			    style: function (feature) {
			        	return {color: 'yellow',weight: 1};
			    }
			}).bindPopup(function (layer) {
			    return 'Terra Indígena (Fonte: Funai)'
					+ ((!!layer.feature.properties.TERRAI_NOME) ? '<br />Nome da Terra Indígena: ' + layer.feature.properties.TERRAI_NOME : '')
					+ ((!!layer.feature.properties.ETNIA_NOME) ? '<br />Etnia: ' + layer.feature.properties.NM_SUBDIST : '')
			}
		);
	";
	return $js;
}

function INFOPNSR_get_quilombos ( $codmun ) {
	require ('config.php');
	$coordinates = INFOPNSR_metabase_get_answer($UUID[49],$codmun);

	$js = "var geojsonFeature = [";
	foreach ($coordinates as $coordinate) {
		$coordinate_dec = json_decode($coordinate->geojson);
		$js .= $coordinate->geojson . ",";
	}
	$js .= "]";

	$js .= "

		var quilombos = L.geoJSON(geojsonFeature,
			{
			    style: function (feature) {
			        	return {color: 'yellow',weight: 1};
			    }
			}).bindPopup(function (layer) {
			    return 'Comuniade Quilombola Demarcada (Fonte: Incra):'
					+ ((!!layer.feature.properties.NM_COMUN3) ? '<br />Nome da Comunidade: ' + layer.feature.properties.NM_COMUN3 : '')
					+ ((!!layer.feature.properties.NR_FAMIL8) ? '<br />Número de Famílias: ' + layer.feature.properties.NR_FAMIL8 : '')
			}
		);
	";
	return $js;
}

function INFOPNSR_get_assentamentos ( $codmun ) {
	require ('config.php');
	$coordinates = INFOPNSR_metabase_get_answer($UUID[48],$codmun);

	$js = "var geojsonFeature = [";
	foreach ($coordinates as $coordinate) {
		$coordinate_dec = json_decode($coordinate->geojson);
		$js .= $coordinate->geojson . ",";
	}
	$js .= "]";

	$js .= "

		var assentamentos = L.geoJSON(geojsonFeature,
			{
			    style: function (feature) {
			        	return {color: 'yellow',weight: 1};
			    }
			}).bindPopup(function (layer) {
			    return 'Assentamento de Reforma Agrária (Fonte: Incra)'
					+ ((!!layer.feature.properties.NOME_PRO2) ? '<br />Nome do Assentamento: ' + layer.feature.properties.NOME_PRO2 : '')
					+ ((!!layer.feature.properties.NUM_FAMI6) ? '<br />Número de Famílias: ' + layer.feature.properties.NUM_FAMI6 : '')
			}
		);

		var overlayMaps = {
		    \"Assentamentos\": assentamentos,
				\"Terras Indígenas\": TI,
				\"Quilombos\": quilombos,
		};

		L.control.layers([],overlayMaps).addTo(mymap);

	";
	return $js;
}
