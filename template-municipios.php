<?php
/*
Template Name: Municipios
*/
get_header();
?>
<!-- Global Bar -->
<style><?php echo '.button-custom { padding: 15px 20px 15px !important; }'; ?></style>
<div class="jumbotron INFOPNSR_municipios_header">
  <div class="jumbotron-bg-color" style="padding: 70px 0;">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 padding-jumbotron">
          <h1 id="glob-msg-box" class="bigtext"><?php echo "5571 brasis"; //$theme_options['home-header-main-title']; ?></h1>
          <p class="titletag_dec"><?php echo "Os municípios são os titulares dos serviços públicos de saneamento básico, ou seja, possuem por competência e responsabilidade a prestação desses serviços. São, portanto, a unidade territorial chave para a efetivação do direito à água, ao esgotamento sanitário, ao manejo adequado dos resíduos sólidos e das águas das chuvas."//$theme_options['home-header-sub-title']; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row INFOPNSR_municipios_search">
  <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 search-margin-top">
    <div class="global-search home">
      <?php get_template_part( 'search', 'home' ); ?>
    </div>
  </div>
</div>
<?php
  $content_post = get_post(get_the_ID());
  $content = $content_post->post_content;
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  echo $content;
?>
<?php get_footer(); ?>
