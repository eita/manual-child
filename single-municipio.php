<?php
/**
 * The template for displaying all single posts and attachments
 */
if ($_GET['download']) {
    INFOPNSR_metabase_get_csv ( $chart_content[$_GET['question']]['uuid'], $_GET['codmun'] );
}
if ($_GET['print']) {
    $options = INFOPNSR_c3_pie( $_GET['codmun'], $_GET['question'], "Bla", array('censo_2010'), $chart_content[$_GET['question']]['uuid'],TRUE );
    INFOPNSR_c3_print_chart ($options);
}
get_header();

$offset = '';
$col_md_sm = 12;
$col_md_sm = 10;
$offset = 'col-md-offset-1';
?>

<header>
  <div class="INFOPNSR_header">
    <div class="INFOPNSR_headertitle">
      <?php the_title( '<h2 class="singlepg-font-blog-upper">', '</h2>' ); ?>
    </div>
    <div class="INFOPNSR_headersearch">
      <?php INFOPNSR_searchbox(); ?>
    </div>
  </div>
  <?php if (!wp_is_mobile()): ?>
  <div id="mapid"></div>
  <?= INFOPNSR_get_the_source( array('censo_2010_malhas', 'pnsr_rural') ); ?>
  <?php endif ?>
</header>

<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row">
<div class="col-md-<?php echo $col_md_sm; ?> col-sm-<?php echo $col_md_sm; ?> <?php echo $offset; ?>">
  <?php
		// Start the loop.
		while ( have_posts() ) : the_post();
			get_template_part( 'content', 'municipio' );
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		// End the loop.
		endwhile;
		?>
  <div class="clearfix"></div>
</div>
<?php get_footer(); ?>
