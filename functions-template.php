<?php
function the_municipio_template( $codmun ) {
	require ('config.php');

	$time_pre = microtime(true);
	echo '
	  <div class="panel-group" id="accordion"> ' .
    INFOPNSR_texto_municipio ($codmun, array('mma', 'atlas_brasil', 'snis')) .
	  INFOPNSR_collapsible_section ("rural", "#67CC98", "Caracterização Demográfica",
			INFOPNSR_template_chart ( 10 , $codmun, $chart_content[10]['title'] , $chart_content[10]['type'] , $chart_content[10]['text'] ) .
			// INFOPNSR_template_chart ( 21 , $codmun, $chart_content[21]['title'] , $chart_content[21]['type'] , $chart_content[21]['text'] ) .
			INFOPNSR_template_chart ( 14 , $codmun, $chart_content[14]['title'] , $chart_content[14]['type'] , $chart_content[14]['text'] )
	  ) .
	  INFOPNSR_collapsible_section ("saneamento", "#F6799B", "Saneamento",
			INFOPNSR_template_chart ( 26 , $codmun, $chart_content[26]['title'] , $chart_content[26]['type'] , $chart_content[26]['text'] ) .
			INFOPNSR_template_chart ( 31 , $codmun, $chart_content[31]['title'] , $chart_content[31]['type'] , $chart_content[31]['text'] ) .
			INFOPNSR_template_chart ( 32 , $codmun, $chart_content[32]['title'] , $chart_content[32]['type'] , $chart_content[32]['text'] ) .
			INFOPNSR_template_chart ( 27 , $codmun, $chart_content[27]['title'] , $chart_content[27]['type'] , $chart_content[27]['text'] )
	  ) .
		INFOPNSR_collapsible_section ("com_tradicionais", "#53C2E6", "Comunidades Tradicionais",
			INFOPNSR_template_chart ( 22 , $codmun, $chart_content[22]['title'] , $chart_content[22]['type'] , $chart_content[22]['text'] , $chart_content[22]['source'] ,$chart_content[22]['noresults']) .
			INFOPNSR_template_chart ( 9 , $codmun, $chart_content[9]['title'] , $chart_content[9]['type'] , $chart_content[9]['text'] , $chart_content[9]['source'] ,$chart_content[9]['noresults'])
	  ) .
		INFOPNSR_collapsible_section ("saude", "#908EC3", "Condições de Saúde",
			INFOPNSR_template_chart ( 94 , $codmun, $chart_content[94]['title'] , $chart_content[94]['type'] , $chart_content[94]['text'])
	  ) .
		INFOPNSR_collapsible_section ("escola", "#d6c544", "Saneamento nas Escolas",
			INFOPNSR_template_chart ( 304 , $codmun, $chart_content[304]['title'] , $chart_content[304]['type'] , $chart_content[304]['text']) .
			INFOPNSR_template_chart ( 215 , $codmun, $chart_content[215]['title'] , $chart_content[215]['type'] , $chart_content[215]['text']) .
			INFOPNSR_template_chart ( 214 , $codmun, $chart_content[214]['title'] , $chart_content[214]['type'] , $chart_content[214]['text']) .
			INFOPNSR_template_chart ( 305 , $codmun, $chart_content[305]['title'] , $chart_content[305]['type'] , $chart_content[305]['text']) .
			INFOPNSR_template_chart ( 216 , $codmun, $chart_content[216]['title'] , $chart_content[216]['type'] , $chart_content[216]['text']) .
			INFOPNSR_template_chart ( 303 , $codmun, $chart_content[303]['title'] , $chart_content[303]['type'] , $chart_content[303]['text'])
	  ) .
		INFOPNSR_collapsible_section ("qualidade_da_agua", "#908EC3", "Qualidade da Água",
			INFOPNSR_template_chart ( 330 , $codmun, $chart_content[330]['title'] , $chart_content[330]['type'] , $chart_content[330]['text'], $chart_content[330]['source'], $chart_content[330]['noresults']).			
			INFOPNSR_template_chart ( 328 , $codmun, $chart_content[328]['title'] , $chart_content[328]['type'] , $chart_content[328]['text'], $chart_content[328]['source'], $chart_content[328]['noresults'])
	  ) .
	  '</div><!-- closing panel-group -->';
		$time_post = microtime(true);
		$exec_time = $time_post - $time_pre;
		// echo "---Panels: $exec_time---";
	return true;

}

function the_estado_template( $coduf ) {

	echo '
	  <div class="panel-group" id="accordion"> ' .
    // INFOPNSR_texto_municipio ($codmun, array('mma', 'atlas_brasil', 'snis')) .
	  INFOPNSR_collapsible_section ("rural", "#67CC98", "Caracterização do Meio Rural",NULL
			// INFOPNSR_template_chart ( 10 , $codmun, "População Urbana e Rural" , 'pie' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada." ) .
		  // INFOPNSR_template_chart ( 21 , $codmun, "Indígenas nas Zonas Rural e Urbana" , 'pie' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada.") .
			// INFOPNSR_template_chart ( 14 , $codmun, "Raça e Cor nas Zonas Rural e Urbana", 'bar_stacked_100' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada.")
	  ) .
	  INFOPNSR_collapsible_section ("saneamento", "#F6799B", "Saneamento Rural",
			INFOPNSR_template_chart ( 39 , $coduf, "Filtro d'Água" , 'bar_stacked_100' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada." )
			// INFOPNSR_template_chart ( 31 , $codmun, "Esgotamento Sanitário" , 'bar_stacked_100' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada." ) .
			// INFOPNSR_template_chart ( 32 , $codmun, "Domicílios Sem Banheiro" , 'bar_stacked_100' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada." ) .
			// INFOPNSR_template_chart ( 27 , $codmun, "Destinação do Lixo" , 'bar_stacked_100' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada." )
	  ) .
		INFOPNSR_collapsible_section ("com_tradicionais", "#53C2E6", "Comunidades Tradicionais",NULL
			// INFOPNSR_template_chart ( 22 , $codmun, "Comunidades Quilombolas Reconhecidas" , 'table' , "txt" , array('fcp')) .
			// INFOPNSR_template_chart ( 9 , $codmun, "População em Terras Indígenas" , 'number' , "txt" , array('censo_2010'))
	  ) .
		INFOPNSR_collapsible_section ("saude", "#908EC3", "Condições de Saúde",NULL
	    // INFOPNSR_template_chart ( 12 , $codmun, "Taxa de Mortalidade Infantil" , 'line' , "Os municípios são responsáveis por elaborar os planos de saneamento básico, organizar e prestar – diretamente ou sob o regime de concessão ou permissão –os serviços públicos de interesse local. Também é responsabilidade municipal a organização, regulação e fiscalização da prestação desses serviços, de forma direta ou delegada." )
	  ) .
	  '</div><!-- closing panel-group -->';

	return true;

}


function INFOPNSR_texto_municipio ($codmun, $source) {
	require ('config.php');

	$time_pre = microtime(true);

	$IDHM = INFOPNSR_metabase_get_answer($UUID[11],$codmun)[0]->IDHM;
	$nome = INFOPNSR_metabase_get_answer($UUID[13],$codmun)[0]->Nome_do_municipio;
	$estado = INFOPNSR_metabase_get_answer($UUID[29],$codmun)[0]->Nome_da_UF;
	$biomas = INFOPNSR_metabase_get_answer($UUID[19],$codmun)[0];
	foreach ($biomas as $bioma => $percentual) {
		if ($percentual > 0)
			$biomas_text[] = $percentual . "% pelo bioma " . $bioma;
	}
	$biomas_text = implode(", ", $biomas_text);
	$lastcomma = strrpos($biomas_text,",",-1); //find the last comma
	if ($lastcomma)
		$biomas_text[$lastcomma] = "#";
	$biomas_text = str_replace("#", " e", $biomas_text);
	$semiarido = INFOPNSR_metabase_get_answer($UUID[30],$codmun)[0]->semiarido;
	$semiarido_text = ($semiarido == 1) ? "$nome é um município do Semiárido Brasileiro.":"";

	$politica = (INFOPNSR_metabase_get_answer($UUID[17],$codmun)[0]->PO001 == "Não") ? "não":"";
	$plano = (INFOPNSR_metabase_get_answer($UUID[18],$codmun)[0]->PO028 == "Não") ? "não":"";

	$text = "<p>$nome é um município da unidade federativa $estado. Seu território é composto $biomas_text. $semiarido_text O IDHM de $nome é $IDHM. O município $politica possui Política Municipal de Saneamento Básico e $plano possui Plano Municipal de Saneamento Básico.</p>";

	$time_post = microtime(true);
	$exec_time = $time_post - $time_pre;
	// echo "---Texto: $exec_time---";

	return $text . INFOPNSR_get_the_source( $source );
}

function INFOPNSR_collapsible_section ( $id, $color, $title, $body ) {
  return '
  	<div class="panel panel-default">
			<a data-toggle="collapse" data-parent="#accordion" href="#' . $id . '">
	  		<div class="panel-heading" style="background: ' . $color . '">
					<i class="fa fa-plus" aria-hidden="true"></i>
	  			<h4 class="panel-title">
	  				' . $title . '
	  			</h4>
	  		</div>
			</a>
			<div id="' . $id . '" class="panel-collapse collapse">
				<div class="panel-body">
	        ' . $body . '
	      </div>
	    </div>
  	</div>
    ';
}

function INFOPNSR_template_chart ( $question, $codmun, $title, $chart_type, $text, $source = NULL, $noresults = NULL ){
	require ('config.php');

	switch ($chart_type) {
		case 'line':
			$text_col_class = $chart_col_class = "col-md-12";
			$chart = '
				<div class="chart" id="plot' . $question . '"></div>
				<div class="INFOPNSR_download_png"><a href="#" id="imgdownload' . $question . '" ><i class="fa fa-image" aria-hidden="true"></i></a></div>';
			break;
		case 'table':
			$chart = INFOPNSR_table( $codmun, $question, $title, $source, $noresults, $chart_content[$question]['uuid']);
			$text_col_class = $chart_col_class = "col-md-12";
			$style = 'style="margin-top:0px !important"';
			break;
		case 'number':
			$chart = INFOPNSR_number( $codmun, $question, $title, $source, $noresults, $chart_content[$question]['uuid']);
			$text_col_class = $chart_col_class = "col-md-12";
			$style = 'style="margin-top:0px !important"';
			break;
		default:
			$chart = '
				<div class="chart" id="plot' . $question . '"></div>
				<div class="INFOPNSR_download_png"><a href="#" id="imgdownload' . $question . '" ><i class="fa fa-image" aria-hidden="true"></i></a></div>
				';
			$chart_col_class = "col-md-8";
			$text_col_class = "col-md-4";
			break;
	}
	return '
  <div class="row">
    <div class="' . $chart_col_class . '" id="chartcol' . $question . '">
			' . $chart . '
			<canvas style="display:none;width:500px;height:500px" id="canvas' . $question . '"></canvas>
			<div style="display:none;" id="png-container' . $question . '"></div>
			<div class="INFOPNSR_download_data"><a href="./?download=csv&codmun=' . $codmun . '&question=' . $question . '"><i class="fa fa-download" aria-hidden="true"></i></a></div>
    </div>
		<div class=' . $text_col_class . '>
      <!--<h1>' . $title . '</h1>-->
      <p class="INFOPNSR_chartdescription" ' . $style . '>' . $text . '</p>
    </div>
  </div>
	';
}
